#!/bin/sh

EXE="/bin/xrandr"
ENABLEDARGS="--preferred --auto"
PRIMARY="eDP-1"
SECONDARY="DP-1"

$EXE --output $PRIMARY $ENABLEDARGS

case $1 in
    "above" )
        $EXE --output $SECONDARY $ENABLEDARGS --above $PRIMARY;;
    "below" )
        $EXE --output $SECONDARY $ENABLEDARGS --below $PRIMARY;;
    "left" )
        $EXE --output $SECONDARY $ENABLEDARGS --left-of $PRIMARY;;
    "right" )
        $EXE --output $SECONDARY $ENABLEDARGS --right-of $PRIMARY;;
    "same" )
        $EXE --output $SECONDARY $ENABLEDARGS --same-as $PRIMARY;;
    "off" )
        $EXE --output $SECONDARY --off;;
    * )
        echo "above | below | left | right | same | off"
esac

